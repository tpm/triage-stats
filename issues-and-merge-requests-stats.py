#!/usr/bin/env python3
#
# issues-and-merge-requests-stats:
# Author: Tim-Philipp Müller
#
# docs: https://python-gitlab.readthedocs.io/en/stable/api-objects.html
from datetime import datetime
import gitlab
import json
import os
import sys
import tempfile

markdown = False
config = None
group_name = 'gstreamer'

stats_filename = 'gstreamer_gitlab_triage_stats.json'

# Sorted by the order in which we want things on a per-project basis output
projects = [
  'gstreamer', 'gst-plugins-base', 'gst-plugins-good', 'gst-plugins-bad',
  'gst-plugins-ugly', 'gst-libav', 'gst-rtsp-server', 'gstreamer-vaapi',
  'gstreamer-sharp', 'gst-python', 'gst-omx', 'gst-editing-services',
  'gst-devtools', 'gst-integration-testsuites', 'gst-docs', 'gst-examples',
  'gst-build', 'gst-ci', 'cerbero',
]

def print_help():
  print('issues-and-merge-requests-stats: Count open merge requests and issues')
  print('')
  print('Usage: {} [OPTIONS]'.format(sys.argv[0]))
  print('')
  print('\t--markdown\tOutput markdown instead of plain text')
  print('\t--identity=CONFIG\tUse identity/configuration CONFIG (from ~/.python-gitlab.cfg)')
  print('\t--group=GROUP\Path of gitlab group (default: gstreamer)')
  print('\t--projects=PROJ1,PROJ2\tList of projects to process (must be in the group)')
  print('\t--ignore-nonexistant-milestone\tSkip project if specified milestone could not be found')
  print('')
  sys.exit(-1)

if '--markdown' in sys.argv:
  markdown = True
  sys.argv.remove('--markdown')

for arg in sys.argv:
  if arg.startswith('--identity'):
    split_arg = arg.split('=')
    if len(split_arg) != 2 or split_arg[1] == '':
      print('Expected --identity=CONFIG, got {}'.format(arg))
      print()
      print_help()
    config = split_arg[1]
    sys.argv.remove(arg)
    break

for arg in sys.argv:
  if arg.startswith('--group'):
    split_arg = arg.split('=')
    if len(split_arg) != 2 or split_arg[1] == '':
      print('Expected --group=GROUP, got {}'.format(arg))
      print()
      print_help()
    group_name = split_arg[1]
    sys.argv.remove(arg)
    break

for arg in sys.argv:
  if arg.startswith('--projects'):
    split_arg = arg.split('=')
    if len(split_arg) != 2 or split_arg[1] == '':
      print('Expected --projects=PROJ1,PROJ2, got {}'.format(arg))
      print()
      print_help()
    projects = split_arg[1].split(',')
    sys.argv.remove(arg)
    break

if len(sys.argv) != 1 or '--help' in sys.argv:
  print_help()

##############################################################################

stats = {}

today_stats = {}

# Read existing stats if any
if os.path.isfile(f'{stats_filename}'):
  print(f'Reading previous stats from {stats_filename}..')
  with open(f'{stats_filename}', 'r') as f:
    previous_stats = json.load(f)
    #print(json.dumps(previous_stats, indent=4))
    stats = previous_stats

gl = gitlab.Gitlab.from_config(config, [os.path.expanduser('~/.python-gitlab.cfg')])
gl.auth()

server_name = gl.user.web_url.split('/')[2]
print('Running as user {} (@{}) on {}'.format(gl.user.name, gl.user.username, server_name))

date_stamp = datetime.utcnow().isoformat()[0:19] + 'Z'
print(f"{'Date': <30}: {date_stamp}")

# Group MRs and issues on a per-project basis for nicer output
for project_name in projects:
  project = gl.projects.get(f'{group_name}/{project_name}')

  # Get list of merge requests and issues
  issues = project.issues.list(state='opened', all=True)
  mrs = project.mergerequests.list(state='opened', all=True)

  n_issues = len(issues)
  n_mrs = len(mrs)

  print(f'{project_name : <30}: {n_issues} issues, {n_mrs} merge requests')

  project_stat_now = {'date': date_stamp, 'n_issues': n_issues, 'n_mrs': n_mrs}
  try:
    project_stats = stats[project_name]  
  except:
    project_stats = []

  project_stats += [project_stat_now]

  stats.update({project_name: project_stats}) 
  today_stats.update({project_name: [project_stat_now]})

print()

# Write out today's stats into a separate file that's not going to be touched again 
with open(f'{date_stamp}-{stats_filename}', 'w') as f:
  print(json.dumps(today_stats, indent=4), file=f)

# Write out previous stats with today's stats added into stats file
with open(f'{stats_filename}', 'w') as f:
  print(json.dumps(stats, indent=4), file=f)
